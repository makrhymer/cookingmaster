﻿
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class ApplicationManager : MonoBehaviour
{
    public static ApplicationManager Manager;
    public CustomerManager mCustomerManager;

    //  UI Elements
    public GameObject Menu;
    public GameObject StartGameButton;
    public GameObject WinnerScreen;
    public Text WinMessageText;
    public GameObject HighscorePanel;
    public GameObject HighscoreEntry;

    //  Scene Elements
    public GameObject Scene;
    public Player BluePlayer;
    public Player RedPlayer;

    //  Game State
    private HighscoreList mHighscores;
    public bool mIsGameInProgress;
    public bool mIsGamePaused;

    void Awake()
    {
        Manager = this;
        mIsGameInProgress = false;
        mIsGamePaused = true;
        mHighscores = new HighscoreList();
    }

    void Start()
    {
        if (File.Exists(Configuration.HIGHSCORE_SAVE_FILE))
        {
            string savedScores = File.ReadAllText("Highscores.json");
            mHighscores = JsonUtility.FromJson<HighscoreList>(savedScores);
        }

        Menu.SetActive(true);
    }

    void Update()
    {
        if(mIsGameInProgress)
        {
            //  Both players ran out of time. So end competition and show the winner screen
            if (BluePlayer.mTimer == 0.0f && RedPlayer.mTimer == 0.0f)
                EndGame();

            //  If only one of the players runs out of time, shift them outside the play area
            if (BluePlayer.mTimer == 0.0f)
                BluePlayer.gameObject.transform.position = Configuration.BLUE_PLAYER_REST_POSITION;
            if (RedPlayer.mTimer == 0.0f)
                RedPlayer.gameObject.transform.position = Configuration.RED_PLAYER_REST_POSITION;
        }

        if (Input.GetKey(KeyCode.Escape))
        {
            if (mIsGameInProgress)
                Util.GetChildByName(StartGameButton, "Text").GetComponent<Text>().text = "RESUME GAME";
            else
                Util.GetChildByName(StartGameButton, "Text").GetComponent<Text>().text = "START GAME";

            mIsGamePaused = true;
            Menu.SetActive(true);
        }
    }

    public void StartNewGame()
    {
        //  Remove customers and reset players
        mCustomerManager.Reset();
        BluePlayer.ResetPlayer(Configuration.BLUE_PLAYER_SPAWN_POSITION, Configuration.GAME_DURATION);
        RedPlayer.ResetPlayer(Configuration.RED_PLAYER_SPAWN_POSITION, Configuration.GAME_DURATION);

        // Reset all interactables in the scene
        IInteractable[] allInteractables = Scene.GetComponentsInChildren<IInteractable>();
        foreach (IInteractable interactable in allInteractables)
            interactable.Reset();

        // Remove all powerups that are still lying around
        Powerup[] allPowerups = Scene.GetComponentsInChildren<Powerup>();
        foreach (Powerup powerup in allPowerups)
            Destroy(powerup.gameObject);

        ResumeGame();
    }

    public void ResumeGame()
    {
        Menu.SetActive(false);
        mIsGamePaused = false;
        mIsGameInProgress = true;
    }

    public void OnButtonStart()
    {
        if (mIsGameInProgress)
            ResumeGame();
        else
            StartNewGame();
    }

    public void OnButtonExit()
    {
        Application.Quit();
    }

    public void OnButtonResetHighscores()
    {
        mHighscores = new HighscoreList();
        foreach (Transform child in HighscorePanel.transform)
            if (child.gameObject.activeSelf)
                Destroy(child.gameObject);
    }

    public void OnButtonReturnToMenu()
    {
        WinnerScreen.SetActive(false);
        Menu.SetActive(true);
    }

    private void EndGame()
    {
        //  Save new scores to high score list
        mHighscores.AddHighScore(BluePlayer);
        mHighscores.AddHighScore(RedPlayer);
        File.WriteAllText(Configuration.HIGHSCORE_SAVE_FILE, JsonUtility.ToJson(mHighscores));

        //  Remove previous high score entries
        foreach (Transform child in HighscorePanel.transform)
            if (child.gameObject.activeSelf)
                Destroy(child.gameObject);

        //  Add high score entries according to updated high scores
        foreach(Highscore score in mHighscores.mScores)
        {
            GameObject entry = GameObject.Instantiate(HighscoreEntry);
            entry.SetActive(true);

            Util.GetChildByName(entry, "Name").GetComponent<Text>().text = score.mPlayerName;
            Util.GetChildByName(entry, "Score").GetComponent<Text>().text = score.mScore.ToString();

            entry.transform.SetParent(HighscorePanel.transform, false);
            entry.SetActive(true);
        }

        //  Display winner message
        if (BluePlayer.mScore > RedPlayer.mScore)
            WinMessageText.text = "BLUE CHEF WINS!!";
        else if (RedPlayer.mScore > BluePlayer.mScore)
            WinMessageText.text = "RED CHEF WINS!!";
        else
            WinMessageText.text = "IT IS A DRAW!!";

        //  Show the winner screen
        WinnerScreen.SetActive(true);
        mIsGameInProgress = false;
        mIsGamePaused = true;
    }
}
