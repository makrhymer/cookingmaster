﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CustomerManager : MonoBehaviour
{
    public GameObject[] mCustomerSeats = new GameObject[5];

    public void Reset()
    {
        foreach (GameObject seat in mCustomerSeats)
        {
            GameObject customer = Util.GetChildByName(seat, "Customer");
            if (customer)
                GameObject.Destroy(customer);
        }
    }

    void Update()
    {
        if (ApplicationManager.Manager.mIsGamePaused)
            return;

        foreach (GameObject seat in mCustomerSeats)
        {
            GameObject customerObject = Util.GetChildByName(seat, "Customer");
            if(customerObject)
            {
                //  Remove customers who have not been served in alloted time
                Customer customer = customerObject.GetComponent<Customer>();

                if (customer.mIsServed || customer.mIsLeaving)
                    Destroy(customerObject, 1.0f);
            }
            else
            {
                //  Bring in a new customer to fill the empty seat
                GameObject newCustomer = CustomerFactory.Factory.Get();
                newCustomer.transform.position = seat.transform.position;
                newCustomer.transform.parent = seat.transform;
            }
        }
    }
}
