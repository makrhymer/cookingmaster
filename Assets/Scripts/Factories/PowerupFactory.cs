﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupFactory : MonoBehaviour
{
    public static PowerupFactory Factory;

    public GameObject Scene;
    public GameObject PowerupSpawnArea;
    public GameObject[] PowerPrefabs = new GameObject[3];

    private void Awake()
    {
        Factory = this;
    }
    
    public GameObject Get(Player player)
    {
        GameObject selectedPowerup= PowerPrefabs[Random.Range(0, PowerPrefabs.Length)];
        GameObject newPowerup = Instantiate(selectedPowerup);
        newPowerup.name = selectedPowerup.name;
        newPowerup.tag = player.tag;

        if (player.CompareTag("Red"))
            newPowerup.GetComponent<SpriteRenderer>().color = Color.red;
        else
            newPowerup.GetComponent<SpriteRenderer>().color = Color.blue;

        Bounds spawnAreaBounds = PowerupSpawnArea.GetComponent<Renderer>().bounds;
        newPowerup.transform.position = spawnAreaBounds.center + new Vector3(Random.Range(0, spawnAreaBounds.extents.x), Random.Range(0, spawnAreaBounds.extents.y));

        newPowerup.transform.parent = Scene.transform;
        return newPowerup;
    }
}
