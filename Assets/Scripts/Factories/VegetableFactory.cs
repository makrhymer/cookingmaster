﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VegetableFactory : MonoBehaviour
{
    public static VegetableFactory Factory;
    public GameObject[] VegetablePrefabs = new GameObject[6];

    private void Awake()
    {
        Factory = this;
    }

    public GameObject Get()
    {
        GameObject selectedVegetable = VegetablePrefabs[Random.Range(0, VegetablePrefabs.Length)];
        GameObject newVegetable = Instantiate(selectedVegetable);
        newVegetable.name = selectedVegetable.name;
        return newVegetable;
    }
}
