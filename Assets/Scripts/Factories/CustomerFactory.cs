﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerFactory : MonoBehaviour
{
    public static CustomerFactory Factory;
    public GameObject Prefab_Customer;
    public Sprite[] mCustomerSprites = new Sprite[15];

    private void Awake()
    {
        Factory = this;
    }

    public GameObject Get()
    {
        //  Create a unique customer
        GameObject newCustomerObject = Instantiate(Prefab_Customer);
        newCustomerObject.name = "Customer";
        newCustomerObject.GetComponent<SpriteRenderer>().sprite = mCustomerSprites[Random.Range(0, mCustomerSprites.Length)];

        //  Create a custom order. Add two vegetables at least and may be a third one as well.
        CustomerOrder order = new CustomerOrder();
        order.mVegetable1 = VegetableFactory.Factory.Get();
        order.mVegetable2 = VegetableFactory.Factory.Get();
        if(Random.Range(0, 10) % 2 == 0)
            order.mVegetable3 = VegetableFactory.Factory.Get();

        //  Each customer has a different max wait time before they run out of patience and leave
        Customer newCustomer = newCustomerObject.GetComponent<Customer>();
        newCustomer.Initialize(order, Random.Range(Configuration.MIN_CUSTOMER_WAIT, Configuration.MAX_CUSTOMER_WAIT));

        return newCustomerObject;
    }
}
