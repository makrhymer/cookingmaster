﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public GameObject PickSlot1;
    public GameObject PickSlot2;
    public GameObject ChoppingNotification;

    public Text TimerText;
    public Text ScoreText;

    public float mTimer;
    public int mScore;

    private IInteractable mCurrentInteractable;
    private GameObject mPickedObject1;
    private GameObject mPickedObject2;

    private float mSpeedBoostTimer;
    private float mChoppingTimer;

    public void ResetPlayer(Vector3 position, float timer)
    {
        mScore = 0;
        mTimer = timer;
        mSpeedBoostTimer = 0.0f;
        mChoppingTimer = 0.0f;

        mCurrentInteractable = null;
        mPickedObject1 = null;
        mPickedObject2 = null;

        transform.position = position;
        UpdatePickedSprites();
    }

    void Update()
    {
        if (ApplicationManager.Manager.mIsGamePaused)
            return;

        //  Update all timers
        mTimer = Mathf.Max(0.0f, mTimer - Time.deltaTime);
        mSpeedBoostTimer = Mathf.Max(0.0f, mSpeedBoostTimer - Time.deltaTime);
        mChoppingTimer = Mathf.Max(0.0f, mChoppingTimer - Time.deltaTime);

        //  Update UI for score and time remaining
        TimerText.text = Util.FormatTime(mTimer);
        ScoreText.text = "Points: " + mScore.ToString();

        //  Move player only if not chopping
        if(mChoppingTimer == 0.0f)
        {
            HandleInput();
            ChoppingNotification.SetActive(false);
        }
        else
        {
            ChoppingNotification.SetActive(true);
        }
            
    }

    private void HandleInput()
    {
        if (ApplicationManager.Manager.mIsGamePaused)
            return;

        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        float speedBoost = 1.0f;
        if (mSpeedBoostTimer > 0.0f)
            speedBoost = Configuration.POWERUP_SPEED_BOOST_VALUE;

        if (tag == "Blue")
        {
            if (Input.GetKey(KeyCode.A)) rb.AddForce(Vector3.left * speedBoost);
            if (Input.GetKey(KeyCode.D)) rb.AddForce(Vector3.right * speedBoost);
            if (Input.GetKey(KeyCode.W)) rb.AddForce(Vector3.up * speedBoost);
            if (Input.GetKey(KeyCode.S)) rb.AddForce(Vector3.down * speedBoost);
            if (Input.GetKeyDown(KeyCode.Q)) this.gameObject.GetComponent<Player>().ProcessPick();
            if (Input.GetKeyDown(KeyCode.E)) this.gameObject.GetComponent<Player>().ProcessDrop();
        }

        if (tag == "Red")
        {
            if (Input.GetKey(KeyCode.LeftArrow)) rb.AddForce(Vector3.left * speedBoost);
            if (Input.GetKey(KeyCode.RightArrow)) rb.AddForce(Vector3.right * speedBoost);
            if (Input.GetKey(KeyCode.UpArrow)) rb.AddForce(Vector3.up * speedBoost);
            if (Input.GetKey(KeyCode.DownArrow)) rb.AddForce(Vector3.down * speedBoost);
            if (Input.GetKeyDown(KeyCode.LeftBracket)) this.gameObject.GetComponent<Player>().ProcessPick();
            if (Input.GetKeyDown(KeyCode.RightBracket)) this.gameObject.GetComponent<Player>().ProcessDrop();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        IInteractable interactable = collision.gameObject.GetComponent<IInteractable>();
        if (interactable != null)
        {
            mCurrentInteractable = interactable;
            interactable.OnPlayerEnter(this);
        }   
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        IInteractable interactable = collision.gameObject.GetComponent<IInteractable>();
        if (interactable != null)
        {
            mCurrentInteractable = null;
            interactable.OnPlayerExit(this);
        }
    }

    public void ProcessPick()
    {
        //  No interactable to pick from
        if (mCurrentInteractable == null)
            return;

        //  Both hands are full. Cannot pick more items
        if (mPickedObject1 != null && mPickedObject2 != null)
            return;

        GameObject picked = mCurrentInteractable.OnPick(this);
        if(picked)
        {
            if (mPickedObject1 == null)
                mPickedObject1 = picked; 
            else
                mPickedObject2 = picked;     
        }

        UpdatePickedSprites();
    }

    public void ProcessDrop()
    {
        //  No interactable to drop to
        if (mCurrentInteractable == null)
            return;

        //  Nothing to drop
        if (mPickedObject1 == null)
            return;

        if(mCurrentInteractable.OnDrop(this, mPickedObject1))
        {
            mPickedObject1 = null;

            if (mPickedObject2 != null)
            {
                mPickedObject1 = mPickedObject2;
                mPickedObject2 = null;
            }
        }

        UpdatePickedSprites();
    }

    private void UpdatePickedSprites()
    {
        if (mPickedObject1)
            PickSlot1.GetComponent<SpriteRenderer>().sprite = mPickedObject1.GetComponent<SpriteRenderer>().sprite;
        else
            PickSlot1.GetComponent<SpriteRenderer>().sprite = null;

        if (mPickedObject2)
            PickSlot2.GetComponent<SpriteRenderer>().sprite = mPickedObject2.GetComponent<SpriteRenderer>().sprite;
        else
            PickSlot2.GetComponent<SpriteRenderer>().sprite = null;
    }

    public void AddScore(int points)
    {
        mScore += points;
    }

    public void AddTime(float seconds)
    {
        mTimer += seconds;
    }

    public void SpeedBoost()
    {
        mSpeedBoostTimer += Configuration.POWERUP_SPEED_BOOST_TIME;
    }

    public void StartChopping()
    {
        mChoppingTimer = Configuration.CHOPPING_TIME;

        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.velocity = Vector2.zero;
        rb.angularVelocity = 0.0f;
    }
}
