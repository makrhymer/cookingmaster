﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChoppingBoard : MonoBehaviour, IInteractable
{
    public GameObject SampleSalad;
    public GameObject ScrambledSalad1;
    public GameObject ScrambledSalad2;
    public GameObject ScrambledSalad3;

    private List<GameObject> mCurrentVegetables;

    void Start()
    {
        mCurrentVegetables = new List<GameObject>();
    }

    public void Reset()
    {
        mCurrentVegetables.Clear();
        UpdateScrambledSalad();
    }

    public void OnPlayerEnter(Player player)
    {
        if (player.tag != tag)
            return;

        Util.GetChildByName(this.gameObject, "Highlight").SetActive(true);
    }

    public void OnPlayerExit(Player player)
    {
        if (player.tag != tag)
            return;

        Util.GetChildByName(this.gameObject, "Highlight").SetActive(false);
    }

    public GameObject OnPick(Player player)
    {
        //  There is nothing on the chopping board to pick up
        if (mCurrentVegetables.Count == 0)
            return null;

        GameObject salad = Instantiate(SampleSalad);
        salad.name = "Salad";

        foreach (GameObject vegetable in mCurrentVegetables)
            vegetable.transform.parent = salad.transform;

        //  Empty the chopping board
        mCurrentVegetables.Clear();
        UpdateScrambledSalad();

        return salad;
    }

    public bool OnDrop(Player player, GameObject droppedObject)
    {
        //  Cannot place vegetables in the other player's plate
        if (player.tag != tag)
            return false;

        //  Cannot place the salad on the chopping board
        if (droppedObject.name == "Salad")
            return false;

        //  Cannot place more than 3 vegetables on the chopping board
        if (mCurrentVegetables.Count > 2)
            return false;

        mCurrentVegetables.Add(droppedObject);
        UpdateScrambledSalad();
        player.StartChopping();
        return true;
    }

    public void UpdateScrambledSalad()
    {
        ScrambledSalad1.SetActive(false);
        ScrambledSalad2.SetActive(false);
        ScrambledSalad3.SetActive(false);

        if (mCurrentVegetables.Count == 1)
            ScrambledSalad1.SetActive(true);
        else if (mCurrentVegetables.Count == 2)
            ScrambledSalad2.SetActive(true);
        else if (mCurrentVegetables.Count == 3)
            ScrambledSalad3.SetActive(true);
        
    }
}
