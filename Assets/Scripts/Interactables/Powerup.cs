﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerup : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == tag)
        {
            Destroy(gameObject);

            Player collectingPlayer = collision.gameObject.GetComponent<Player>();
            if(collectingPlayer)
            {
                if (this.gameObject.name == "PowerupScore")
                    collectingPlayer.AddScore(Configuration.POWERUP_SCORE_GAIN);
                if (this.gameObject.name == "PowerupTime")
                    collectingPlayer.AddTime(Configuration.POWERUP_TIMER_GAIN);
                if (this.gameObject.name == "PowerupSpeed")
                    collectingPlayer.SpeedBoost();
            }
        }
    }
}
