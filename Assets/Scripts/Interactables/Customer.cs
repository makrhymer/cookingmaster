﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerOrder
{
    public GameObject mVegetable1 = null;
    public GameObject mVegetable2 = null;
    public GameObject mVegetable3 = null;
}

public class Customer : MonoBehaviour, IInteractable
{
    public GameObject WaitBarFilled;
    public GameObject OrderSlot1;
    public GameObject OrderSlot2;
    public GameObject OrderSlot3;
    public GameObject AngryFace;

    public float mMaxWaitTime;
    public float mWaitTime;
    public bool mIsServed;
    public bool mIsLeaving;

    private CustomerOrder mOrder;

    private bool mIsAngry;
    private List<Player> mAngryWith;
    
    public void Initialize(CustomerOrder order, float maxWaitTime)
    {
        mOrder = order;
        mMaxWaitTime = maxWaitTime;
        mWaitTime = 0.0f;

        mIsServed = false;
        mIsLeaving = false;
        mIsAngry = false;
        mAngryWith = new List<Player>();

        //  Show customer order on their card
        OrderSlot1.GetComponent<SpriteRenderer>().sprite = mOrder.mVegetable1.GetComponent<SpriteRenderer>().sprite;
        OrderSlot2.GetComponent<SpriteRenderer>().sprite = mOrder.mVegetable2.GetComponent<SpriteRenderer>().sprite;
        if(mOrder.mVegetable3)
            OrderSlot3.GetComponent<SpriteRenderer>().sprite = mOrder.mVegetable3.GetComponent<SpriteRenderer>().sprite;
    }

    void Update()
    {
        if (ApplicationManager.Manager.mIsGamePaused)
            return;

        //  Update wait time. Angry customers get frustrated at double the rate of a happy customer
        mWaitTime += Time.deltaTime;
        if (mIsAngry)
            mWaitTime += Time.deltaTime;

        //  Leave if maximum wait time has been reached
        float percentageWaited = mWaitTime / mMaxWaitTime;
        if (percentageWaited > 1.0f)
        {
            percentageWaited = 1.0f;
            if (mIsLeaving == false)
                Leave();
        }

        //  Update the wait time indicator
        WaitBarFilled.transform.localScale = new Vector3(2.0f - (percentageWaited * 2), 3, 1);
    }

    public void Reset()
    {
        //  Customers are created on scene reset automatically. So no processing required for existing customers.
    }

    public void OnPlayerEnter(Player player)
    {
        Util.GetChildByName(this.gameObject, "Highlight").SetActive(true);
    }

    public void OnPlayerExit(Player player)
    {
        Util.GetChildByName(this.gameObject, "Highlight").SetActive(false);
    }

    public GameObject OnPick(Player player)
    {
        //  You cannot pick anything from a customer's table! Which culinary school did you go to??
        return null;
    }

    public bool OnDrop(Player player, GameObject droppedObject)
    {
        if(droppedObject.name != "Salad")
        {
            //  If you give the customer anything other than a salad, that is a big no no :)
            GetAngryAt(player);
            return true;
        }
        else
        {
            //  Check if salad served matches the customer's order
            GameObject find1 = Util.GetChildByName(droppedObject, mOrder.mVegetable1.name);
            if (find1 == null)
            {
                GetAngryAt(player);
                return true;
            }
            find1.transform.parent = null;

            GameObject find2 = Util.GetChildByName(droppedObject, mOrder.mVegetable2.name);
            if (find2 == null)
            {
                GetAngryAt(player);
                return true;
            }
            find2.transform.parent = null;

            if(mOrder.mVegetable3 != null)
            {
                GameObject find3 = Util.GetChildByName(droppedObject, mOrder.mVegetable3.name);
                if (find3 == null)
                {
                    GetAngryAt(player);
                    return true;
                }
                find3.transform.parent = null;
            }
            
            //  If we reach this point, then the order has been successfully delivered
            player.AddScore(Configuration.POINTS_CORRECT_ORDER);

            //  Give reward if served within 70% of alloted time
            if (mWaitTime / mMaxWaitTime < 0.7f)
                PowerupFactory.Factory.Get(player);

            mIsServed = true;
            return true;
        }
    }

    private void GetAngryAt(Player player)
    {
        mIsAngry = true;
        mAngryWith.Add(player);
        AngryFace.SetActive(true);
    }

    public void Leave()
    {
        mIsLeaving = true;

        ApplicationManager.Manager.RedPlayer.AddScore(Configuration.PENALTY_MISSED_ORDER);
        ApplicationManager.Manager.BluePlayer.AddScore(Configuration.PENALTY_MISSED_ORDER);

        if (mIsAngry)
        {
            foreach (Player player in mAngryWith)
                player.AddScore(Configuration.PENALTY_WRONG_ORDER);
        }
    }
}
