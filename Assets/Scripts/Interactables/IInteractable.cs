﻿
using UnityEngine;

//  Any game object that wants to interact with the player need to implement this interface
interface IInteractable
{
    void Reset();
    void OnPlayerEnter(Player player);
    void OnPlayerExit(Player player);
    GameObject OnPick(Player player);
    bool OnDrop(Player player, GameObject droppedObject);
}
