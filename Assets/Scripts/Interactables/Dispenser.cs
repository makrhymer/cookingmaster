﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dispenser : MonoBehaviour, IInteractable
{
    public GameObject mVegetable;
    public GameObject[] mVegetableSlots = new GameObject[6];

    void Start()
    {
        foreach(GameObject vegetableSlot in mVegetableSlots)
            vegetableSlot.GetComponent<SpriteRenderer>().sprite = mVegetable.GetComponent<SpriteRenderer>().sprite;
    }

    public void Reset() { }

    public void OnPlayerEnter(Player player)
    {
        Util.GetChildByName(this.gameObject, "Highlight").SetActive(true);
    }

    public void OnPlayerExit(Player player)
    {
        Util.GetChildByName(this.gameObject, "Highlight").SetActive(false);
    }

    public GameObject OnPick(Player player)
    {
        GameObject newVegetable = Instantiate(mVegetable);
        newVegetable.name = mVegetable.name;
        return newVegetable;
    }

    public bool OnDrop(Player player, GameObject droppedObject)
    {
        if (droppedObject.name == mVegetable.name)
            return true;
        return false;
    }
}
