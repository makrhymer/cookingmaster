﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bin : MonoBehaviour, IInteractable
{
    public void Reset() {}

    public void OnPlayerEnter(Player player)
    {
        Util.GetChildByName(this.gameObject, "Highlight").SetActive(true);
    }

    public void OnPlayerExit(Player player)
    {
        Util.GetChildByName(this.gameObject, "Highlight").SetActive(false);
    }

    public GameObject OnPick(Player player)
    {
        //  Taking things out of the garbage bin is not a good idea. Just forget about it :)
        return null;
    }

    public bool OnDrop(Player player, GameObject droppedObject)
    {
        //  Penalize player for wasting perfectly good vegetables
        if (droppedObject.name == "Salad")
            player.AddScore(Configuration.PENALTY_SALAD_THROWN_TO_BIN);

        //  The bin accepts everything that comes its way
        return true;
    }
}
