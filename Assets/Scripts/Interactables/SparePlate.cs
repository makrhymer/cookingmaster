﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SparePlate : MonoBehaviour, IInteractable
{
    public GameObject VegetableSlot;
    private GameObject mCurrentVegetable;

    void Start()
    {
        Reset();
    }

    public void Reset()
    {
        mCurrentVegetable = null;
        VegetableSlot.GetComponent<SpriteRenderer>().sprite = null;
    }

    public void OnPlayerEnter(Player player)
    {
        if (player.tag != tag)
            return;

        Util.GetChildByName(this.gameObject, "Highlight").SetActive(true);
    }

    public void OnPlayerExit(Player player)
    {
        if (player.tag != tag)
            return;

        Util.GetChildByName(this.gameObject, "Highlight").SetActive(false);
    }

    public GameObject OnPick(Player player)
    {
        if(mCurrentVegetable)
        {
            GameObject mReturnVeg = mCurrentVegetable;
            mCurrentVegetable = null;
            VegetableSlot.GetComponent<SpriteRenderer>().sprite = null;
            return mReturnVeg;
        }
        else
        {
            return null;
        }
    }

    public bool OnDrop(Player player, GameObject droppedObject)
    {
        //  The plate can only hold one vegetable
        if (mCurrentVegetable)
            return false;

        //  Cannot place vegetables in the other player's plate
        if (player.tag != tag)
            return false;

        //  Cannot place the salad on the spare plate
        if (droppedObject.name == "Salad")
            return false;

        mCurrentVegetable = droppedObject;
        VegetableSlot.GetComponent<SpriteRenderer>().sprite = droppedObject.GetComponent<SpriteRenderer>().sprite;
        return true;
    }
}
