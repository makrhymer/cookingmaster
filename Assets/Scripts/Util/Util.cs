﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Util
{
    public static string FormatTime(float seconds)
    {
        int intTime = (int)seconds;
        string formattedTime = (intTime / 60).ToString("D2") + ":";
        formattedTime += (intTime % 60).ToString("D2");
        return formattedTime;
    }

    public static GameObject GetChildByName(GameObject gameObject, string childName)
    {
        Transform childTransform = gameObject.transform.Find(childName);
        if (childTransform != null)
            return childTransform.gameObject;
        return null;
    }
}
