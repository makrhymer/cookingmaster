﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Configuration
{
    public static int GAME_DURATION = 200;
    public static float MIN_CUSTOMER_WAIT = 50.0f;
    public static float MAX_CUSTOMER_WAIT = 70.0f;
    public static float CHOPPING_TIME = 2.0f;

    public static int POINTS_CORRECT_ORDER = 100;
    public static int POWERUP_SCORE_GAIN = 100;
    public static int POWERUP_TIMER_GAIN = 20;
    public static int POWERUP_SPEED_BOOST_TIME = 20;
    public static float POWERUP_SPEED_BOOST_VALUE = 1.5f;

    public static int PENALTY_MISSED_ORDER = -50;
    public static int PENALTY_WRONG_ORDER = -50;
    public static int PENALTY_SALAD_THROWN_TO_BIN = -50;

    public static Vector3 BLUE_PLAYER_REST_POSITION = new Vector3(-7.3f, 3.0f, 27);
    public static Vector3 RED_PLAYER_REST_POSITION = new Vector3(6.1f, 3.0f, 27);
    public static Vector3 BLUE_PLAYER_SPAWN_POSITION = new Vector3(-4, 0, 27);
    public static Vector3 RED_PLAYER_SPAWN_POSITION = new Vector3(3, 0, 27);

    public static string HIGHSCORE_SAVE_FILE = "Highscores.json";
}
