﻿
using System.Collections.Generic;
using System;

[Serializable]
public class Highscore
{
    public string mPlayerName;
    public int mScore;

    public Highscore(string playerName, int score)
    {
        mPlayerName = playerName;
        mScore = score;
    }
}

[Serializable]
public class HighscoreList
{
    public List<Highscore> mScores;

    public HighscoreList()
    {
        mScores = new List<Highscore>();
    }

    public void AddHighScore(Player player)
    {
        mScores.Add(new Highscore(player.name, player.mScore));

        mScores.Sort(delegate (Highscore one, Highscore two)
        {
            if (one.mScore > two.mScore)
                return -1;
            return 1;
        });

        if (mScores.Count > 10)
            mScores.RemoveAt(10);
    }
}